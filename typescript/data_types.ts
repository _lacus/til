//// Data types

// Bool

let isDone: boolean = false

// Numbers

let decimal: number = 6;
let hex: number = 0xf00d;
let binary: number = 0b1010;
let octal: number = 0o744;

// Strings

let color: string = "blue";
name = 'red';

// Template string

let fullName: string = `Bob Bobbington`;
let age: number = 37;
let sentence: string = `Hello, my name is ${ fullName }.

I'll be ${ age + 1 } years old next month.`

// Array

let list: number[] = [1, 2, 3];
// list[1] = "Oh shit!"; // ERROR

// Tuple
// fixed number and type of elements

let x: [string, number];
// Initialize it
x = ['hello', 10]; // OK
// x = [10, 'hello']; // Error
// x[1] = "word"; // Error
x[0] = "word" // OK
x[2] = "word" // OK
// x[6] = true // ERROR -> [string, number]

// Any

let notSure: any = 4;
notSure = "maybe a string instead";
notSure = false; // okay, definitely a boolean

let list2: any[] = [1, true, "free"];
list2[1] = 100;

// Void
// You can only assign undefined or null to void type variables

let unusable: void = undefined;
let unusable2: void = null;