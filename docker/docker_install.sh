# docker installer - Simple installer for ubuntu
# for install into a vagrant box set in vagrantfile: config.vm.provision :shell, :path => "docker_install.sh"

# Select user to add docker group:
USER=vagrant
# Select ubuntu version:
# Precise 12.04 (LTS) > ubuntu-precise
# Trusty 14.04 (LTS) > ubuntu-trusty
# Wily 15.10 > ubuntu-wily
UBUNTU_VERSION=ubuntu-wily

sudo apt-get -q -y install apt-transport-https ca-certificates
sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D

sudo echo deb https://apt.dockerproject.org/repo $UBUNTU_VERSION main > /etc/apt/sources.list.d/docker.list

sudo apt-get -q -y update
sudo apt-get -q -y purge lxc-docker -y
sudo apt-cache policy docker-engine

sudo apt-get -q -y update
sudo apt-get -q -y install docker-engine docker-compose
sudo usermod -aG docker $USER
