>>> Plugins to install
atom-aligment
atom-beautify
auto-detect-indentation
linter
script
csscomb
css-watch
pigments
minimap
keyboard-localization

>>> Themes
seti-ui

>>> To Disable
metrics
language-java
language-clojure
language-c
language-objective-c
