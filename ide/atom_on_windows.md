'''It's easy to set Atom as default editor.'''

1. Go to Windows Explorer
2. Select a .txt file, right click, go to Open with, then Choose default program
3. In dialog, copy and paste this: %LOCALAPPDATA%\atom\bin\atom.cmd and click OK
