### For loop

# 0-9
for a in range(10):
    print a

# 1-10
for a in range(1, 11):
    print a

# 10-1
for a in range(10, 0, -1):
    print a

my_dict = {'a':1, 'b':2, 'c':3}
for key in my_dict:
    print key, "=", my_dict[key]
