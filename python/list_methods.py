'''Common list methods'''

list.append(x)      # Add an item to the end of the list. Equivalent to a[len(a):] = [x].
list.extend(L)      # Extend the list by appending all the items in the given list. Equivalent to a[len(a):] = L.
list.insert(i, x)   # Insert an item at a given position. The first argument is the index of the element to insert.
list.remove(x)      # Remove the first item from the list whose value is x. It is an error if there is no such item.
list.copy()         # Return a shallow copy of the list. Equivalent to a[:].
list.pop([i])       # Remove the item at the given position and return it. If no index is specified, removes the last item.
list.clear()        # Remove all items from the list. Equivalent to del a[:].
list.index(x)       # Return the index in the list of the first item whose value is x. It is an error if there is no such item.
list.count(x)       # Return the number of times x appears in the list.
list.reverse()      # Reverse the elements of the list in place.
list.sort(key=None, reverse=False) 	# Sort the items of the list in place (the arguments can be used for sort customization).
