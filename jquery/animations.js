/* Animations */

( [duration ] [, easing ] [, complete ] )

// Show/Hide

$('#additional-information').show(750);

$('#additional-information').hide(750);

$('#additional-information').toggle(750);


// Fade in/out

$('#additional-information').fadeIn(750);

$('#additional-information').fadeOut(750);

$('#additional-information').fadeToggle(750);

// Slide down/up

$('#additional-information').slideDown(750);

$('#additional-information').slideUp(750);

$('#additional-information').slideToggle(750);
