/* DOM manipulation */

// Adding a class

currentElement.addClass('class-name class-name2');

// Removing a class

currentElement.removeClass('class-name class-name2');

// Attributes

alert($('selector').attr('attribute-name')); // Retrieving an attribute value

$('selector').attr('attribute-name', 'new value'); // Modifying an attribute value

$('selector').removeAttr('style') // Remove given attribute

// Element content manipulation

$('selector').text('Updated text!') // Modifies text content

$('selector').html('<u>Updated text!</u>') // Modifies children

$('selector').text() // Reads text content

$('selector').html() // Reads children

// css

// Retrieve an item's color
var color = $('#target').css('color');

// Change an item's color to red
$('#target').css('color', 'red');


// The end result of the JavaScript below would be an element with red text and a yellow background.
var style = {
	color: 'red',
	backgroundColor: yellow
};

$('#target').css(style);

// Prepend

// prepend is called on the target, and accepts the new content as the parameter
$('#target').prepend('<div>New content</div>');

// prependTo is called on the new content, and accepts the target as the parameter
$('<div>New content</div>').prependTo('#target');

// Append

// append is called on the target, and accepts the new content as the parameter
$('#target').append('<div>New content</div>');

// appendTo is called on the new content, and accepts the target as the parameter
$('<div>New content</div>').appendTo('#target');


// before

// before is called on the target, and accepts the new content as a parameter
$('target').before('New content');

// insertBefore is called on the new content, and accepts the target as a parameter
$('New content').insertBefore('#target');

// After

// after is called on the target, and accepts the new content as a parameter
$('target').after('New content');

// insertAfter is called on the new content, and accepts the target as a parameter
$('New content').insertAfter('#target');

// Wrap content

// The wrap function wraps each item with the element passed into the function.
$('#target').wrap('<section></section>');

// Wraps all returned content with one new element.
$('.demo').wrapAll('<section></section>');

// Wrapping inside content of a element
$('#target').wrapInner('<section></section>');

// Remove, empty

// Removes target from the DOM
$('#target').remove()

// Emty target content
$('#target').empty()

// Replace

// replaceWith replaces the content on the left with the new content in the parameter
$('#target').replaceWith('<div>NEW content</div>');

// replaceAll replaces the target in the parameter with the content on the left
$('<div>NEW content</div>').replaceAll('#target');

// Clone Examples

<button type="button" id="add-line">Add new line</button>
<div id="container">
	<div class="user-entry">
		<label>Email:</label>
		<input type="email" />
		<label>Password:</label>
		<input type="password" />
	</div>
</div>
 $(function() {
 	$userForm = $('.user-entry').clone;
	$('#add-line').click(function() {
		$('#container').append($userForm.clone());
	});
 });
