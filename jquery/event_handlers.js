/* Event handlers */

// Inside of the event handler code, you can access the object that raised the event by using this. One important note about this is it is not a jQuery object but rather a DOM object; you can convert it by using the jQuery constructor as we've seen before: $(this).

// click event
// raised when the item is clicked
$(item).click(function() {
    alert('clicked!!');
});

// hover event
// raised when the user moves their mouse over the item
$(item).hover(function() {
    alert('hover!!');
});

// mouseout
// raised when the user moves their mouse away from an item
$(item).mouseout(function() {
    alert('mouse out');
});

// doubleclick
$('#target').dblclick(function() {
    alert('hello, world!');
});

// Form events
<!-- sample HTML -->
<div>
    <label for="phone">Phone</label>
    <input type="text" id="phone" />
    <span id="phone-help"></span>
</div>
// sample JavaScript
$('#phone').focus(function() {
    // Control has focus. Display help
    $('#phone-help').text('Please enter your phone number as all digits');
}).blur(function() {
    // Control lost focus. Clear help
    $('#phone-help').text('');
});


/* Examples */

$('button').click(function() {
    // this is linked to button that was clicked, but is a DOM object
    // convert it to a jQuery object by using the jQuery factory
    $(this).text('Clicked!!');
});

// Write out the x/y coordinates of the mouse on click
$('button').click(function(e) {
    $('#output').text(e.pageX + 'x' + e.pageY);
});


/* Registering event handlers */

$('#demo').on('click', function() { alert('hello!!'); });
$('#demo').click(function() { alert('hello!!'); });

/* Delegate event handlers */

$(document).delegate('button', 'click', function() {
    alert('hello');
});

/* Single event execution */

$('#single').one('click', function() {
    $('#output').text('You clicked on the button');
});

/* Execute event with trigger */

// goes trough all matching elements
$('button').trigger('click');

// Executes only the first matching elements click function
$('button').triggerHandler('click')
