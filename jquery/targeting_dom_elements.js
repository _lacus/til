// Targeting DOM elements

// Targeting by css selectors

$('h1') // selects all h1 elements

$('.class-name') // selects all elements with a class of class-name

$('#demo') // selects all elements with an id of demo

// Targeting by atribute

$('[demo-attribute="demo-value"]')  // selects **all** elements with an attribute matching the specified value

$('h1[demo-attribute="demo-value"]') // selects all **h1** elements with an attribute matching the specified value

$('[class^="col"]') // If you wish to find all elements where the value starts with a string, use the ^= operator.

$('[class*="md"]') // If you wish to find all elements where the value contains a string, use the *= operator.

// Targeting by position

$('nav > a') // Selects all a elements that are direct descendants nav element

$('nav a') // Selects all a elements that are descendants nav element

// Walk down

$('...').children('.child') // > .child

$('...').find('.child') // will match all children (children of children)

// Walk up

$('...').parent('.parent') // Select the parent of a element

$('...').parents('.parent') // Selects all  parents (Parent of parents)

// Select on the same level

$('...').siblings() // Get the siblings of each element in the set of matched elements, optionally filtered by a selector.

$('...').prev() // Previous siebling

$('...').prevAll() // All previous siebling

$('...').prevUntil('.stop')  // All previous siebling until a element

$('...').next() // Previous siebling

$('...').nextAll() // All previous siebling

$('...').nextUntil('.stop')  // All previous siebling until a element

// Target by index

index = $('...').index('.target') // Get the index of a target

$('...').get(index) // Return DOM element by index
