// Document ready

//There are a couple of ways to register this event handler. The first is to access the document object in jQuery, and register an event handler with the jQuery ready event.

$(document).ready(function() {
	// code here
});

//Because registering an event handler with jQuery is so common, jQuery provides a shortcut. Simply passing a function into the constructor will register that function as the event handler for document.load.

$(function() {
	// code here
});
