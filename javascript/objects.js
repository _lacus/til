// Create object via literal notation

var Spencer = {
  age: 22,
  country: "United States"
};

// Create object via constructor

var bob = new Object();
bob.name = "Bob Smith";
bob.age = 30;

// Accessing Properties via dot notation

var bob = {
  name: "Bob Smith",
  age: 30
};

var name1 = bob.name;
var age1 = bob.age;

// Accessing Properties via bracket notation

var dog = {
  species: "greyhound",
  weight: 60,
  age: 4
};

var species = dog["species"];
// fill in the code to save the weight and age using bracket notation
var weight = dog["weight"]
var age = dog["age"]
